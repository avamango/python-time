#!/usr/bin/python

#Convert entered time into 24-hour integer format to facilitate math functions.
def convertTime(z1, z2, z3):
    z1 = int(z1)
    z2 = int(z2)
    if z3 == 'pm' and z1 < 12:
        z1 = z1 + 12
    if z3 == 'am' and z1 == 12:
    	z1 = 0
    return (z1, z2)


#Check the input that goes into the math functions.
def checkInput(x):
	while True:
		try:
			x.isdigit()
			x = int(x)

		except:
			return False
			break

		if (x >= 0 and x <= 600):
			return True
			break

		else:
			return False


def add(t0Hour, t0Min, addHour, addMin):
	try:
		t0Hour = int(t0Hour)
		t0Min = int(t0Min)
		addHour = int(addHour)
		addMin = int(addMin)
	
	except:
		print 'FAIL'
	
	t1Min = t0Min + addMin
	t1Hour = t0Hour + addHour
	
	if t1Min >= 60:
		t1Hour = t1Hour + (t1Min / 60)
		t1Min = t1Min % 60
	
	t1Day = 0
	if t1Hour > 24:
		t1Day = t1Hour / 24
		t1Hour = t1Hour % 24
	
	return t1Day, t1Hour, t1Min


def minus(t1Hour, t1Min, minusHour, minusMin):
	try:
		t1Hour = int(t1Hour)
		t1Min = int(t1Min)
		minusHour = int(minusHour)
		minusMin = int(minusMin)

	except:
		print 'FAIL'

	t0Min = t1Min - minusMin
	t0Hour = t1Hour - minusHour

	if t0Min < 0 and t0Min >= -60:
		t0Hour = t0Hour - 1
		t0Min = 60 + t0Min
	
	elif t0Min < -60:
		t0Hour = t1Hour + (t0Min / 60)
		t0Min = -t0Min % 60

	if t0Hour < 0 and t0Hour >= -24:
		t0Day = -1
		t0Hour = 24 + t0Hour
	
	elif t0Hour < -24:
		t0Day = (t0Hour / 60)
		t0Hour = -t0Hour % 60

	else:
		t0Day = 0

	return t0Day, t0Hour, t0Min


def diffTime(t0H, t0M, t1D, t1H, t1M):
	try:
		t0H = int(t0H)
		t0M = int(t0M)
		t1D = int(t1D)
		t1H = int(t1H)
		t1M = int(t1M)
	
	except:
		print 'FAIL'

	if t1D > 0:
		rHour = ((24 * t1D) + t1H) - t0H
		rDay = rHour / 24
		rHour = rHour % 24

	else:
		rDay = 0
		rHour = t1H - t0H
	
	if t1M < t0M:
		rHour = rHour - 1
		rMin = t1M + 60
		rMin = rMin - t0M

	else:
		rMin = t1M - t0M
		 
	return (rDay, rHour, rMin)

#print diffTime(20, 0, 5, 6, 0)