#!/usr/bin/python

#Split the user's input into 3 parts.
def parser(x):
    x1 = x[0:2]
    x2 = x[3:5]
    x3 = x[6:8]
    
    if not x1.isalnum():
        x1 = x[0:1]
        x2 = x[2:4]
        x3 = x[5:7]
    
    return (x1, x2, x3)

#Basic check on the input user gives for time.
def timeCheck(x):
    while True:
        if len(x) == 7 or len(x) == 8:
            x1 = parser(x)[0]
            x2 = parser(x)[1]
            x3 = parser(x)[2]
            
            try:
                x1.isdigit()
                x2.isdigit()
                x3.isalpha()
        
            except:
                return False
                break

            if (len(x1) == 1 or len(x1) == 2 and
                len(x2) == 2 and len(x3) == 2 and
                (str(x3[0:1]) == 'a' or str(x3[0:1]) == 'p') and
                str(x3[1:2]) == 'm' and int(x1) >= 1 and int(x1) <= 12
                and int(x2) >= 0 and int(x2) <= 60):
                return True
                break
            
            else:
                return False
                break
        
        else:
            return False
            break

#Show the current calculated time.
def displayTime(message, dd, hh, mm):
    hh = int(hh)
    if hh == 0:
        hh = 12
        zz = 'am'
    elif hh == 12:
        zz = 'pm'
    elif hh > 12:
        hh = hh - 12
        zz = 'pm'
    else:
        zz = 'am'
    dd = str(dd)    
    hh = str(hh)
    mm = str(mm)
    if len(mm) == 1:
        mm = '0' + mm
    print '\n' + message + dd + ' day(s) ' + hh + ":" + mm + ' ' + zz + '\n'