#!/usr/bin/python

#Author: Avnish Mangal
#E-mail: avnishmangal@gmail.com


from src import TimeFn, Math
#####################################
print '\n***************************'
print '*                         *'
print '*  Welcome to Time Math!  *'
print '*                         *'
print '***************************\n'
print 'Note: Please enter all times in format HH:MM DD\n'
print '  ex. 4:20 pm\n'
#####################################


#Choose function.
print 'Choose from the following:'
print '[1] Add time'
print '[2] Subtract time'
print '[3] Find the difference between two times'
tput = raw_input('\nYour choice: ')
while True:
    try:
        tput.isdigit()
    except:
        tput = raw_input('Enter 1, 2, or 3: ')
    
    if (tput == '1' or tput == '2' or tput == '3'):
        break
    
    else:
        tput = raw_input('Enter 1, 2, or 3: ')


# [Addition]
if int(tput) == 1:
    y = raw_input('Enter starting time (T0): ')
    
    while True:
        if TimeFn.timeCheck(y):
            hh = TimeFn.parser(y)[0]
            mm = TimeFn.parser(y)[1]
            zz = TimeFn.parser(y)[2]
            hh = Math.convertTime(hh, mm, zz)[0]
            mm = Math.convertTime(hh, mm, zz)[1]
            break
        
        else:
            y = raw_input('No. Re-enter: ')

    hourAdd = raw_input('\nHow many [hours] to add?: ')
    while True:
        if Math.checkInput(hourAdd):
            break
        else:
            print 'Please enter in correct format.'
            hourAdd = raw_input('\nHow many [hours] to add?: ')

    minAdd = raw_input('\nHow many [minutes] to add?: ')
    while True:
        if Math.checkInput(minAdd):
            break
        else:
            print 'Please enter in correct format.'
            minAdd = raw_input('\nHow many [minutes] to add?: ')
    
    Math.add(hh, mm, hourAdd, minAdd)
    day = Math.add(hh, mm, hourAdd, minAdd)[0]
    hour = Math.add(hh, mm, hourAdd, minAdd)[1]
    minutes = Math.add(hh, mm, hourAdd, minAdd)[2]

    print '\n'
    TimeFn.displayTime('T1 = ', day, hour, minutes)


# [Subtraction]
if int(tput) == 2:
    y = raw_input('Enter starting time (T1): ')
    
    while True:
        if TimeFn.timeCheck(y):
            hh = TimeFn.parser(y)[0]
            mm = TimeFn.parser(y)[1]
            zz = TimeFn.parser(y)[2]
            hh = Math.convertTime(hh, mm, zz)[0]
            mm = Math.convertTime(hh, mm, zz)[1]
            break
        
        else:
            y = raw_input('No. Re-enter: ')

    hourMinus = raw_input('\nHow many [hours] to subtract?: ')
    while True:
        if Math.checkInput(hourMinus):
            break
        else:
            print 'Please enter in correct format.'
            hourMinus = raw_input('\nHow many [hours] to subtract?: ')

    minMinus = raw_input('\nHow many [minutes] to subtract?: ')
    while True:
        if Math.checkInput(minMinus):
            break
        else:
            print 'Please enter in correct format.'
            minMinus = raw_input('\nHow many [minutes] to subtract?: ')
    
    Math.minus(hh, mm, hourMinus, minMinus)
    day = Math.minus(hh, mm, hourMinus, minMinus)[0]
    hour = Math.minus(hh, mm, hourMinus, minMinus)[1]
    minutes = Math.minus(hh, mm, hourMinus, minMinus)[2]

    print '\n'
    TimeFn.displayTime('T0 = ', day, hour, minutes)


if int(tput) == 3:
    y = raw_input('Enter starting time (T0): ')
    while True:
        if TimeFn.timeCheck(y):
            t0H = TimeFn.parser(y)[0]
            t0M = TimeFn.parser(y)[1]
            t0zz = TimeFn.parser(y)[2]
            t0H = Math.convertTime(t0H, t0M, t0zz)[0]
            t0M = Math.convertTime(t0H, t0M, t0zz)[1]
            break
        
        else:
            y = raw_input('No. Re-enter: ')

    d = raw_input('\nHow many days ahead?: ')
    while True:
        try:
            d.isdigit()
            d = int(d)
        except:
            d = raw_input('Re-enter: ')

        if d >= 0 and d < 100:
            break

        else:
            d = raw_input('Re-enter: ')

    x = raw_input('Enter finishing time (T1): ')
    while True:
        if TimeFn.timeCheck(x):
            t1H = TimeFn.parser(x)[0]
            t1M = TimeFn.parser(x)[1]
            t1zz = TimeFn.parser(x)[2]
            t1H = Math.convertTime(t1H, t1M, t1zz)[0]
            t1M = Math.convertTime(t1H, t1M, t1zz)[1]
            break
        
        else:
            x = raw_input('No. Re-enter: ')

    dD = Math.diffTime(t0H, t0M, d, t1H, t1M)[0]
    dH = Math.diffTime(t0H, t0M, d, t1H, t1M)[1]
    dM = Math.diffTime(t0H, t0M, d, t1H, t1M)[2]

    dD = str(dD)
    dH = str(dH)
    dM = str(dM)

    print '\n\nT1 is: ' + dD + ' day(s), ' + dH + ' hours, and ' + \
        dM + ' minutes away'
##########################
if __name__ == '__main__':

    print '\n\n****************************'
    print '*    Program Terminated    *\n'
